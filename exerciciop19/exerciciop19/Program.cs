﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercicio19
{
    class Program
    {
        static void Main(string[] args)
        {
            
           
            List<string[]> Pessoas = new List<string[]>();
          

            string genero = string.Empty;
            string GostarDoProduto = string.Empty;

          
            while (true)
            {
                string[] Array = new string[2];
                //Genero
                Array[0] = string.Empty;
                //GostarOuNãoGostar
                Array[1] = string.Empty;
                Console.WriteLine("Introduza 0 para sair");
                Console.WriteLine("Qual o seu género? (m/f)");
                genero = Console.ReadLine().ToLower();
                Console.WriteLine("Gostou do produto (s/n)?");
                GostarDoProduto = Console.ReadLine().ToLower();
                if(genero == "0" || GostarDoProduto == "0")
                {
                    break;
                }
                if (genero == "m" || genero == "f")
                {
                    Array[0] = genero;
                }
                if (GostarDoProduto == "s" || GostarDoProduto == "n")
                {
                    Array[1] = GostarDoProduto;
                }
                Pessoas.Add(Array);
            }

            double generoMasculinoNaoGostaram = Pessoas.Count(x => x[0] == "m" && x[1] == "n");
            generoMasculinoNaoGostaram = (generoMasculinoNaoGostaram / Pessoas.Count)*100;
            Console.WriteLine("Total Pessoas {0}", Pessoas.Count);
            Console.WriteLine("Gostaram {0}", Pessoas.Count(x => x[1] == "s"));
            Console.WriteLine("Não gostaram {0}", Pessoas.Count(x => x[1] == "n"));
            Console.WriteLine("Percentagem de pessoas do género masculino que não gostaram do produto {0}%", generoMasculinoNaoGostaram.ToString("0.##"));
           
            int GeneroMasculinoQueGostaram = Pessoas.Count(x => x.ElementAt(0) == "m" && x.ElementAt(1) == "s");
            int GeneroFemininoQueGostaram = Pessoas.Count(x => x.ElementAt(0) == "f" && x.ElementAt(1) == "s");
            if(GeneroFemininoQueGostaram > GeneroMasculinoQueGostaram)
            {
             Console.WriteLine("Informação dizendo em que género o produto teve melhor aceitação: Feminio ");

            }
            else
            {
              Console.WriteLine("Informação dizendo em que género o produto teve melhor aceitação: Masculino ");
                
            }


          

            Console.ReadKey();



        }
    }
}
